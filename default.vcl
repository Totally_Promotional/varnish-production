vcl 4.1;

import goto;
import xbody;
import headerplus;
import crypto;
import std;
import cookieplus;
import urlplus;
import format;
import ykey;
import http;
import uri;
import vsthrottle;

backend default {
        .host = "72.3.194.224";
        .port = "443";
        .ssl = 1;                               # Turn on SSL support
        .ssl_sni = 0;                   # Use SNI extension  (default: 1)
        .ssl_verify_peer = 0;   # Verify the peer's certificate chain (default: 1)
        .ssl_verify_host = 0;   # Verify the host name in the peer's certificate (default: 0)
	.connect_timeout = 5s;
        .first_byte_timeout = 90s;
        .between_bytes_timeout = 10s;
}

backend staging_crap {
        .host = "65.61.149.225";
        .port = "443";
        .ssl = 1;                               # Turn on SSL support
        .ssl_sni = 0;                   # Use SNI extension  (default: 1)
        .ssl_verify_peer = 0;   # Verify the peer's certificate chain (default: 1)
        .ssl_verify_host = 0;   # Verify the host name in the peer's certificate (default: 0)
    	.connect_timeout = 5s;
	.first_byte_timeout = 90s;
	.between_bytes_timeout = 10s;
}

sub vcl_recv {
        unset req.http.x-cache;
}

sub vcl_hit {
        set req.http.x-cache = "hit";
}

sub vcl_miss {
        set req.http.X-Bob = "haha";
        set req.http.x-cache = "miss";
}

sub vcl_pass {
        set req.http.x-cache = "pass";
}

sub vcl_pipe {
        set req.http.x-cache = "pipe uncacheable";
}

sub vcl_synth {
        set req.http.x-cache = "synth synth";
        set resp.http.Cache-Control = "no-store, private, max-age=0";


        if (req.http.X-Redirect) {
                set resp.status = 301;
                set resp.http.Location = req.http.X-Redirect;
                set resp.http.Cache-Control = "public, max-age=30000000";
                set resp.reason = "Moved";
                return (deliver);
        }
        # uncomment the following line to show the information in the response
        # set resp.http.x-cache = req.http.x-cache;
}

sub vcl_deliver {
        if (obj.uncacheable) {
                set req.http.x-cache = req.http.x-cache + " uncacheable" ;
        } else {
                set req.http.x-cache = req.http.x-cache + " cached" ;
        }
        # uncomment the following line to show the information in the response
        set resp.http.x-cache = req.http.x-cache;
}


sub vcl_init {
        # new def_backend = goto.dns_director("https://www.totallypromotional.com/", host_header="www.totallypromotional.com", ssl=true, ssl_sni=false,ip_version=ipv4,port=443);
        # new liquidfire = goto.dns_director("https://casad.liquifire.com/", host_header="casad.liquifire.com", ssl=true, ssl_sni=false,ip_version=ipv4,port=443);

}


sub vcl_recv {

	if (req.esi_level > 0) {
		set req.http.X-Is-ESI = "true";
	}
        # return (pipe);

        unset req.http.X-Internal-Purge;

        if (req.http.Host ~ "staging") {
                set req.backend_hint = staging_crap;
        }

        if (urlplus.query_get("do_esi") == "true") {
                set req.http.X-Do-ESI = true;
        }

        if (req.esi_level > 0) {
                unset req.http.X-Do-ESI;
                set req.http.Inside-ESI = "true";
        }

        set req.http.X-Ban-URL = req.url;
        set req.http.X-Normal-Ban-URL = urlplus.url_as_string();
        if (req.url ~ "/purge/all") {
                if (urlplus.query_get("password") ~ "k4jhtlooEC52OaSh") {
                        set req.http.keys = ykey.purge_keys("all");
                        ban("obj.http.X-Normal-Ban-URL ~ " + "/");
                        return (synth(200, "You have purged " + req.http.keys + " pages"));
                } else {
                        return (synth(401, "You are not allowed to do this"));
                }
        }

        if (req.method ~ "BAN") {
                if (req.http.Authorization ~ "k4jhtlooEC52OaSh") {
                        ban("obj.http.x-ban-url == " +  req.url);
                        # set req.http.X-Internal-Purge = crypto.string(crypto.hash(sha256, std.tolower(urlplus.url_as_string())));
                        # ykey.purge_keys(req.http.X-Internal-Purge);
                        return (synth(200, "You have banned objects in the cache"));
                } else {
                        return (synth(401, "You are not allowed to do this"));
                }
        }

        if (urlplus.query_get("password") ~ "k4jhtlooEC52OaSh" && urlplus.query_get("ban") == "true") {
                ban("obj.http.X-Normal-Ban-URL ~ " + urlplus.url_as_string());
                # set req.http.X-Internal-Purge = crypto.string(crypto.hash(sha256, std.tolower(urlplus.url_as_string())));
                # ykey.purge_keys(req.http.X-Internal-Purge);
                return (synth(200, "You have banned objects in the cache"));
        }
        call do_redirects;
        if (req.url ~ "admin") {
                return (pass);
        }


        if (urlplus.query_get("guiId", "test") != "test") {
                return (pass);
        }
        # if (req.url ~ "Home/Footer") {
        #         return (synth(200, ""));
        # }
        # if (req.http.host ~ "api") {
        #         set req.backend_hint = api_backend.backend();
        #         set req.http.Host = "api.totallypromotional.com";
        # } else {
        #         set req.backend_hint = def_backend.backend();
        #         set req.http.Host = "www.totallypromotional.com";
        # }

         if (req.url ~ "/varnish-images/staging/Data/Media") {
                set req.url = regsub(req.url, "varnish-images/staging/", "");
                // set req.backend_hint = api_backend.backend();
                set req.http.Host = "staging-api.totallypromotional.com";
                set req.backend_hint = staging_crap;
                unset req.http.Cookie;
                return (hash);
        }

        if (req.url ~ "/varnish-images/Data/Media") {
                # if (req.http.Host ~ "staging") {
                #         set req.http.Host = "staging-api.totallypromotional.com";
                # } else {
                        set req.http.Host = "api.totallypromotional.com";
                # }
                set req.url = regsub(req.url, "varnish-images/", "");
                set req.backend_hint = default;
                unset req.http.Cookie;
                // set req.backend_hint = api_backend.backend();
                # set req.http.Host = "api.totallypromotional.com";
                return (hash);
        }

        # if (req.url ~ "/liquidfire") {
        #         set req.url = regsub(req.url, "liquidfire/", "");
        #         // set req.backend_hint = api_backend.backend();
        #         set req.http.Host = "casad.liquifire.com";
        #         set req.backend_hint = liquidfire.backend();
        #         return (hash);
        # }

        if (req.url ~ "(UpdateCartProduct|User|/Product/GetRecentViewProducts)") {
                return (pass);
        }

        if (req.url ~ "^/(Cart|cart)") {
                return (pass);
        }



        # if (req.url ~ "(TPProduct|TPShipping)") {
        #         if (req.http.Referer) {
        #                 uri.parse(req.http.Referer);
        #                 set req.http.X-Internal-Purge = crypto.string(crypto.hash(sha256, std.tolower(uri.as_string("%P"))));
        #                 uri.reset();
        #         }
        # }

        if (req.url ~ "(TPProduct|TPShipping|/tpCategory/getSubCategoriesForMenu/|TPGetBreadCrumb|TPGetFonts|[Mm]edia)") {
                std.cache_req_body(25MB);
                set req.http.X-Body-Hash = crypto.string(crypto.hash(sha256, regsub(xbody.get_req_body(), "&_\=\d+", "")));
                //unset req.http.Body-String;
                set req.http.X-Method = req.method;
                unset req.http.Cookie;
                return (hash);
        }

        if (req.url ~ "(bundles|/Views/Themes/)" || urlplus.url_get() ~ "\.html$") {
                unset req.http.Cookie;
                return (hash);
        }
        return (pass);
}


sub vcl_backend_fetch {
        if (bereq.http.X-Method) {
                set bereq.method = bereq.http.X-Method;
        }

        unset bereq.http.X-Body-Hash;
        unset bereq.http.X-Method;

	if (bereq.http.X-Is-ESI != "true") {
		if (vsthrottle.is_denied(bereq.http.CF-Connecting-IP, 500, 5s, 5m)) {
			# Client has exceeded 500 reqs per 5s.
			# When this happens, block altogether for the next 5m.
			return (error(429, "Too Many Requests"));
		}
	}
}

sub vcl_hash {
        if (req.url ~ "(TPProduct|TPShipping|TPGetBreadCrumb|TPGetFonts|media)") {
                hash_data(req.http.Host);
                urlplus.query_delete("_");
                hash_data(urlplus.as_string());
                hash_data(req.http.X-Body-Hash);
                hash_data(req.http.X-Do-ESI);
                return (lookup);
        }

        hash_data(req.http.X-Do-ESI);
}


sub collectpre {
        #xbody.regsub({"(<a class="btn-select" href="(http[^"'\s]*)")"}, {"<link rel="prefetch" href="\2">\1"}, "g");
        # xbody.capture("prefetchURLs", {"class="btn-select" href="(http[^"'\s]*)""}, {"\1"}, "g");

}

sub prefetch_esi {
        #xbody.regsub({"(<a class="btn-select" href="(http[^"'\s]*)")"}, {"<esi:include src="\2" onerror="continue"/>\1"}, "g");
        # xbody.capture("prefetchURLs", {"class="btn-select" href="(http[^"'\s]*)""}, {"\1"}, "g");

}

sub vcl_backend_response {


        # if (req.http.X-Do-ESI == "true") {
        #         set beresp.do_esi = true;
        # } else {
        #         call collectpre;
        # }

        set beresp.http.X-Normal-Ban-URL = bereq.http.X-Normal-Ban-URL;

        ykey.add_key("all");
        if (bereq.http.X-Internal-Purge) {
                ykey.add_key(bereq.http.X-Internal-Purge);
        }
        set beresp.http.X-Ban-URL = bereq.http.X-Ban-URL;
        unset beresp.http.strict-transport-security;
        if (beresp.http.content-type ~ "text") {
                # xbody.regsub("totallypromotional.com", "unh2021.com");
                xbody.regsub({"@font-face\s*{"}, {"@font-face{font-display:swap;"});
                if (bereq.http.Host ~ "totallypromotional") {
                        if (bereq.http.Host ~ "staging") {
                                xbody.regsub("(staging-){1}api.totallypromotional.com/Data/Media/", "staging.totallypromotional.com/varnish-images/staging/Data/Media/");
                                xbody.regsub("(staging-){0,1}api.totallypromotional.com/Data/Media/", "staging.totallypromotional.com/varnish-images/Data/Media/");
                        } else {
                                xbody.regsub("(staging-){1}api.totallypromotional.com/Data/Media/", "totallypromotional.com/varnish-images/staging/Data/Media/");
                                xbody.regsub("(staging-){0,1}api.totallypromotional.com/Data/Media/", "totallypromotional.com/varnish-images/Data/Media/");
                        }
                } else {
                        if (bereq.http.Host ~ "staging") {
                                 xbody.regsub("(staging-){1}api.totallypromotional.com/Data/Media/", "staging.totallyweddingkoozies.com/varnish-images/staging/Data/Media/");
                                xbody.regsub("(staging-){0,1}api.totallypromotional.com/Data/Media/", "staging.totallyweddingkoozies.com/varnish-images/Data/Media/");
                        } else {
                                xbody.regsub("(staging-){1}api.totallypromotional.com/Data/Media/", "totallyweddingkoozies.com/varnish-images/staging/Data/Media/");
                                xbody.regsub("(staging-){0,1}api.totallypromotional.com/Data/Media/", "totallyweddingkoozies.com/varnish-images/Data/Media/");
                        }
                }
                # xbody.regsub("api.totallypromotional.com/Data/Media/", "api.totallypromotional.com/Data/Media/");
                if (bereq.url ~ "\.html") {
                        # xbody.regsub("<img", {"<img load="lazy""});
                        # xbody.regsub({"TPGlobal\.prototype\.SetupSubItemsInNavigationMenu\(\);"},{"setTimeout(function(){TPGlobal.prototype.SetupSubItemsInNavigationMenu();},300);"});
                        # xbody.regsub("//unh2021.com", "//www.unh2021.com");
                        #xbody.regsub({"https\://totallypromotional\.ladesk\.com/scripts/track\.js"}, "");
                        #xbody.regsub({"\<script src\="https\://fast\.wistia\.com/assets/external/E\-v1\.js" async\>\</script\>
# "}, "");
                        #xbody.regsub({"https\://cookie-cdn\.cookiepro\.com/scripttemplates/otSDKStub\.js"}, "");
                        #xbody.regsub({"https\://cookie-cdn\.cookiepro\.com/consent/7345572f-b579-47f3-b647-4875afb59e02-test/OtAutoBlock\.js"}, "");
                        #xbody.regsub({"\<script async type\="text/javascript" src\="//static\.klaviyo\.com/onsite/js/klaviyo\.js\?company_id\=ssGU6Z"\>\</script\>"}, {""});
                        xbody.regsub({"otSDKStub.js""}, {"otSDKStub.js" async defer"});
                        xbody.regsub({"OtAutoBlock.js""}, {"OtAutoBlock.js" async defer"});

                        # xbody.regsub({"<link[^>]*?href="([^"]+?\.css)"[^>]+?rel="stylesheet"[^>]*?>"}, {"<style data-href="\1"><esi:include src="\1" onerror="continue"/></style>"});
                        # set beresp.do_esi = true;
                        if (bereq.http.Host ~ "staging") {
                                if (bereq.http.Host ~ "totallypromotional") {
                                        xbody.capture("product-image", {"(id="product-image"[^>]*?\ssrc="https\://staging.totallypromotional.com([^"]+?)")"}, "\2", "g");
                                        xbody.capture("imprint-image", {"(id="imprint-image"[^>]*?\ssrc="https\://staging.totallypromotional.com([^"]+?)")"}, "\2", "g");
                                } else {
                                        xbody.capture("product-image", {"(id="product-image"[^>]*?\ssrc="https\://staging.totallyweddingkoozies.com([^"]+?)")"}, "\2", "g");
                                        xbody.capture("imprint-image", {"(id="imprint-image"[^>]*?\ssrc="https\://staging.totallyweddingkoozies.com([^"]+?)")"}, "\2", "g");
                                }
                        } else {
                                if (bereq.http.Host ~ "totallypromotional") {
                                        xbody.capture("product-image", {"(id="product-image"[^>]*?\ssrc="https\://totallypromotional.com([^"]+?)")"}, "\2", "g");
                                        xbody.capture("imprint-image", {"(id="imprint-image"[^>]*?\ssrc="https\://totallypromotional.com([^"]+?)")"}, "\2", "g");
                                } else {
                                        xbody.capture("product-image", {"(id="product-image"[^>]*?\ssrc="https\://totallyweddingkoozies.com([^"]+?)")"}, "\2", "g");
                                        xbody.capture("imprint-image", {"(id="imprint-image"[^>]*?\ssrc="https\://totallyweddingkoozies.com([^"]+?)")"}, "\2", "g");
                                }
                        }
                        xbody.capture("CustomJs", {"(CustomJs\?v=([^"']*))"}, "\2");
                        xbody.capture("CoreJs", {"(CoreJs\?v=([^"']*))"}, "\2");
                        xbody.capture("jquery", {"(jquery\?v=([^"']*))"}, "\2");
                        xbody.capture("siteBundle", {"((weddingKooziesJs|totallyPromotionalJs)\?v=([^"']*))"}, "\3");
                        # xbody.capture("prefetchURLs", {""}, {""}, "g");
                        if (bereq.http.X-Do-ESI == "true") {
                                call prefetch_esi;
                                set beresp.do_esi = true;
                        } else {
                                call collectpre;
                        }
                        # xbody.capture("coreJs", {"(coreJs\?v=[^"']*)"}, "\1");
                        # xbody.capture("product-image", {"(src="|url\(|src=')(.*?)(.*?)("|\)|')"}, "penguin26\3", "g");
                        # xbody.capture("product-image-imprint", {"(id="imprint-image"[^>]+?\ssrc="([^"]+?)")"}, "\2",);
                }
        }

        if (bereq.url ~ "(CoreJs|CustomJs|totallyPromotionalJs|weddingKooziesJs)") {

                xbody.regsub({"n\.setAttribute\("src",window\.location\.protocol\+"//"\+window\.location\.host\+"/scripts/lib/datepicker\.js"\),"}, {""});
                # xbody.regsub({"},20\)"}, {"}, 1)"});
                # xbody.regsub({"setTimeout\(e,t\)"}, {"setTimeout(e,100)"});
                # xbody.regsub({"fast\.wistia\.net/assets/external/E\-v1\.js"}, "");
                # xbody.regsub("1e3", "150");


                # if (bereq.http.Host ~ "totallypromotional") {
                #        # xbody.regsub({"https\://casad\.liquifire\.com/casad"}, {"https://staging.totallypromotional.com/liquidfire/casad"});
                # } else {
                #        # xbody.regsub({"https\://casad\.liquifire\.com/casad"}, {"https://staging.totallyweddingkoozies.com/liquidfire/casad"});
                # }
                # xbody.regsub({"https://casad.liquifire.com/casad", {"https://casad.liquifire.com/casad"}})
                # xbody.regsub("SetupSubItemsInNavigationMenu\s*=\s*function\(\)\s*{", {"SetupSubItemsInNavigationMenu = function() {return;"});
                # xbody.regsub({"t.prototype.populateAccordion\(\);"}, {""});
        }


        # cookieplus.setcookie_regsub("totallypromotional.com", VALUE, "totallypromotional.com", "unh2021.com");


        if (bereq.url ~ "(TPProduct|TPShipping)") {
                set beresp.ttl = 2m;
                unset beresp.http.Set-Cookie;
                return (deliver);
        }



        if (bereq.url ~ "(bundles|getSubCategoriesForMenu|TPGetBreadCrumb|TPGetFonts|\.ttf|\.woff|Home/Footer|/Views/Themes/|[Mm]edia)" || urlplus.url_get() ~ "\.html$") {
                set beresp.ttl = 5d;
                set beresp.http.Cache-Control = "public, max-age=400000,s-maxage=5000000";
                unset beresp.http.pragma;
                unset beresp.http.Set-Cookie;
                return (deliver);
        }


        if (bereq.url ~ "getSubCategoriesForMenu" && beresp.http.Content-Length ~ "^0") {
                return (retry);
        }
}



sub vcl_deliver {
        if (req.url ~ "\.html") {
                headerplus.init(resp);

                if (xbody.get("product-image[0]", "no") != "no") {
                        headerplus.add("Link", format.quick({"<%s>; rel=preload; as=image;"}, xbody.get("product-image[0]")));
                }

                if (xbody.get("imprint-image[0]", "no") != "no") {
                        headerplus.add("Link", format.quick({"<%s>; rel=preload; as=image;"}, xbody.get("imprint-image[0]")));
                }


                headerplus.add("Link", "</bundles/jquery?v=" +  xbody.get("jquery") + ">; rel=preload; as=script;");
                if (req.http.Host ~ "totallypromotional") {
                        headerplus.add("Link", "</Views/Themes/TP_Default/Content/bootstrap-3.3.7/css/bootstrap.min.css>; rel=preload; as=style;");
                        headerplus.add("Link", "</Views/Themes/TP_Default/Content/css/site.css>; rel=preload; as=style;");
                } else {
                        headerplus.add("Link", "</Views/Themes/WeddingKoozies/Content/bootstrap-3.3.7/css/bootstrap.min.css>; rel=preload; as=style;");
                        headerplus.add("Link", "</Views/Themes/WeddingKoozies/Content/css/site.css>; rel=preload; as=style;");
                }
                headerplus.add("Link", "</bundles/CoreJs?v=" +  xbody.get("CoreJs") + ">; rel=preload; as=script;");
                headerplus.add("Link", "</bundles/CustomJs?v=" +  xbody.get("CustomJs") + ">; rel=preload; as=script;");
                if (req.http.Host ~ "totallypromotional") {
                        headerplus.add("Link", "</bundles/totallyPromotionalJs?v=" +  xbody.get("siteBundle") + ">; rel=preload; as=script;");
                } else {
                        headerplus.add("Link", "</bundles/weddingKooziesJs?v=" +  xbody.get("siteBundle") + ">; rel=preload; as=script;");
                }

                headerplus.add("Link", "</Scripts/lib/JsTree/jstree.min.js>; rel=preload; as=script;");
                if (req.http.Host ~ "totallypromotional") {
                        headerplus.add("Link", "</Views/Themes/TP_Default/Scripts/lib/jquery.simpleLens.min.js>; rel=preload; as=script;");
                        headerplus.add("Link", "</Views/Themes/TP_Default/Scripts/lib/jquery.simpleGallery.min.js>; rel=preload; as=script;");
                        headerplus.add("Link", "</Views/Themes/TP_Default/fonts/znode-web-store.ttf?pa46i9>; rel=preload; as=font; type=font/ttf; crossorigin=anonymous;");
                        headerplus.add("Link", "</Views/Themes/TP_Default/Fonts/Century-Gothic.woff>; rel=preload; as=font; type=font/woff; crossorigin=anonymous;");
                        headerplus.add("Link", "</Views/Themes/TP_Default/Fonts/CenturyGothic-Bold.woff>; rel=preload; as=font; type=font/woff; crossorigin=anonymous;");
                } else {
                        headerplus.add("Link", "</Views/Themes/WeddingKoozies/Scripts/lib/jquery.simpleLens.min.js>; rel=preload; as=script;");
                        headerplus.add("Link", "</Views/Themes/WeddingKoozies/Scripts/lib/jquery.simpleGallery.min.js>; rel=preload; as=script;");
                        headerplus.add("Link", "</Views/Themes/WeddingKoozies/fonts/znode-web-store.ttf?pa46i9>; rel=preload; as=font; type=font/ttf; crossorigin=anonymous;");
                        headerplus.add("Link", "</Views/Themes/WeddingKoozies/Fonts/Montserrat-Regular_0.otf>; rel=preload; as=font; type=font/otf; crossorigin=anonymous;");
                        headerplus.add("Link", "</Views/Themes/WeddingKoozies/Fonts/Montserrat-Medium.otf>; rel=preload; as=font; type=font/otf; crossorigin=anonymous;");
                        headerplus.add("Link", "</Views/Themes/WeddingKoozies/Fonts/Montserrat-Bold.otf>; rel=preload; as=font; type=font/otf; crossorigin=anonymous;");
                }

                headerplus.write();
        }
}




sub do_redirects {
        if (req.url ~ "/blog/top-10-wedding-koozie-artwork-designs/") {
                set req.http.X-Redirect = "/blog/wedding-koozie-ideas/";
        } else if (req.url ~ "/blog/7-signs-you-need-promo-products/") {
                set req.http.X-Redirect = "/blog/statistics-on-promotional-products-to-empower-your-marketing-team/";
        } else if (req.url ~ "/blog/can-coolers-are-great-wedding-favors/") {
                set req.http.X-Redirect = "/blog/best-wedding-favors/";
        } else if (req.url ~ "/blog/top-10-wedding-koozie-designs-2015/") {
                set req.http.X-Redirect = "/blog/top-10-wedding-koozie-designs-for-2019/";
        } else if (req.url ~ "/blog/top-ten-totally-wedding-koozie-template-designs-of-2014/") {
                set req.http.X-Redirect = "/blog/top-10-wedding-koozie-designs-for-2019/";
        }


        if (req.http.Host ~ "totallypromotional.com") {
                if (req.url ~ "^/blog/index.php/.*") {
                        set req.http.X-Redirect = regsuball(req.url, "/blog/index.php/(.*)", "https://www.totallypromotional.com/blog/\1");
                } else if (req.url ~ "^/store/full-color-collapsible-wedding-can-cooler\.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-koozies-can-coolers.html";
                } else if (req.url ~ "^/10-oz-wedding-frosted-plastic-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/10-oz-wedding-frosted-plastic-cup.html";
                } else if (req.url ~ "^/12-oz-wedding-colored-frosted-plastic-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/12-oz-wedding-colored-frosted-plastic-cup.html";
                } else if (req.url ~ "^/12-oz-wedding-colored-frosted-plastic-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/12-oz-wedding-colored-frosted-plastic-cup.html";
                } else if (req.url ~ "^/12-oz-wedding-frosted-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/12-oz-wedding-frosted-cup.html";
                } else if (req.url ~ "^/12-oz-wedding-stadium-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/12-oz-wedding-stadium-cup.html";
                } else if (req.url ~ "^/14-oz-wedding-frosted-plastic-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/14-oz-wedding-frosted-plastic-cup.html";
                } else if (req.url ~ "^/16-oz-translucent-wedding-stadium-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/16-oz-translucent-wedding-stadium-cup.html";
                } else if (req.url ~ "^/16-oz-wedding-colored-frosted-plastic-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/16-oz-wedding-colored-frosted-plastic-cup.html";
                } else if (req.url ~ "^/16-oz-wedding-frosted-plastic-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/16-oz-wedding-frosted-plastic-cup.html";
                } else if (req.url ~ "^/16-oz-wedding-mood-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/16-oz-wedding-mood-cup.html";
                } else if (req.url ~ "^/16-oz-wedding-stadium-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/16-oz-wedding-stadium-cup.html";
                } else if (req.url ~ "^/20-oz-wedding-frosted-plastic-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/20-oz-wedding-frosted-plastic-cup.html";
                } else if (req.url ~ "^/22-oz-translucent-wedding-stadium-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/22-oz-translucent-wedding-stadium-cup.html";
                } else if (req.url ~ "^/22-oz-wedding-stadium-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/22-oz-wedding-stadium-cup.html";
                } else if (req.url ~ "^/2oz-classic-acrylic-wedding-shot-glass.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/2oz-classic-acrylic-wedding-shot-glass.html";
                } else if (req.url ~ "^/32-oz-wedding-stadium-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/32-oz-wedding-stadium-cup.html";
                } else if (req.url ~ "^/3ply-cocktail-wedding-napkin-diagonal-imprint.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/3ply-cocktail-wedding-napkin-diagonal-imprint.html";
                } else if (req.url ~ "^/3ply-cocktail-wedding-napkin-square-imprint.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/3ply-cocktail-wedding-napkin-square-imprint.html";
                } else if (req.url ~ "^/3ply-dinner-wedding-napkin-diagonal-imprint.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/3ply-dinner-wedding-napkin-diagonal-imprint.htm";
                } else if (req.url ~ "^/3ply-dinner-wedding-napkin-square-imprint.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/3ply-dinner-wedding-napkin-square-imprint.html";
                } else if (req.url ~ "^/3ply-guest-wedding-towel-square-imprint.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/3ply-guest-wedding-towel-square-imprint.html";
                } else if (req.url ~ "^/3ply-printed-cocktail-wedding-napkin-diagonal-imprint.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/3ply-printed-cocktail-wedding-napkin-diagonal-imprint.html";
                } else if (req.url ~ "^/3ply-printed-cocktail-wedding-napkin-square-imprint.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/3ply-printed-cocktail-wedding-napkin-square-imprint.html";
                } else if (req.url ~ "^/4-round-40pt-pulpboard-wedding-coasters.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/4-round-40pt-pulpboard-wedding-coasters.html";
                } else if (req.url ~ "^/4-round-foam-wedding-coasters.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/4-round-foam-wedding-coasters.html";
                } else if (req.url ~ "^/9-oz-wedding-frosted-plastic-cup.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/9-oz-wedding-frosted-plastic-cup.html";
                }  else if (req.url ~ "^/neoprene-wedding-zippered-bottle-cooler.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/neoprene-wedding-zippered-bottle-cooler.html";
                }  else if (req.url ~ "^/polypropylene-economy-wedding-tote.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/polypropylene-economy-wedding-tote.html";
                }  else if (req.url ~ "^/polypropylene-wedding-tote.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/polypropylene-wedding-tote.html";
                }  else if (req.url ~ "^/wedding-collapsible-foam-12-oz-slim-can-cooler.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-collapsible-foam-12-oz-slim-can-cooler.html";
                }  else if (req.url ~ "^/wedding-favors-and-reception/wedding-can-coolers.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-koozies-can-coolers.html";
                }  else if (req.url ~ "^/wedding-favors-and-reception/wedding-cups.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-cups.html";
                }  else if (req.url ~ "^/wedding-favors-and-reception/wedding-napkins.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-napkins.html";
                }  else if (req.url ~ "^/wedding-foam-can-sleeve.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-foam-can-sleeve.html";
                }  else if (req.url ~ "^/wedding-neoprene-can-cooler.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-neoprene-can-cooler.html";
                }  else if (req.url ~ "^/wedding-premium-foam-can-cooler.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-premium-foam-can-cooler.html";
                }  else if (req.url ~ "^/wedding-zipper-bottle-cooler.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-zipper-bottle-cooler.html";
                }  else if (req.url ~ "^/design-ideas/wedding-designs/wedding-napkins-designs.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-napkins.html";
                } else if (req.url ~ "^/design-ideas/wedding-designs/wedding-cups-designs.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-cups.html";
                } else if (req.url ~ "^/design-ideas/wedding-designs/bachelor-party-designs.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/bachelor-party-favors.html";
                } else if (req.url ~ "^/design-ideas/wedding-designs/bachelorette-party-designs.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/bachelorette-party-favors.html";
                } else if (req.url ~ "^/design-ideas/wedding-designs/bridal-shower-designs.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/bridal-shower-favors.html";
                } else if (req.url ~ "^/design-ideas/wedding-designs/save-the-date-designs.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/save-the-date.html";
                } else if (req.url ~ "^/design-ideas/wedding-designs/wedding-tote-bag-designs.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-tote-bags.html";
                } else if (req.url ~ "^/design-ideas/wedding-designs/wedding-coaster-designs.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-coasters.html";
                } else if (req.url ~ "^/design-ideas/wedding-can-cooler-designs.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-koozies-can-coolers.html";
                } else if (req.url ~ "^/design-ideas/wedding-designs/wedding-can-cooler-designs.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-koozies-can-coolers.html";
                } else if (req.url ~ "^/wedding-favors-and-reception/wedding-tote-bags.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-tote-bags.html";
                } else if (req.url ~ "^/wedding-favors-and-reception.html") {
			set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-favors.html";
		}
        } else if (req.http.Host ~ "totallyweddingkoozies.com") {
                if (req.url ~ "^/blog/index.php/.*") {
                        set req.http.X-Redirect = regsuball(req.url, "/blog/index.php/(.*)", "https://www.totallypromotional.com/blog/\1");
                } else if (req.url ~ "^/blog.*") {
                        set req.http.X-Redirect = "https://www.totallypromotional.com/blog/";
                } else if (req.url ~ "^/store/new-designs.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com";
                } else if (req.url ~ "^/store/white-wedding-party-collapsible-can-cooler.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/store/wedding-party-can-cooler-designs.html";
                } else if (req.url ~ "^/store/wedding-collapsible-neoprene-can-insulator.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-neoprene-can-cooler.html";
                } else if (req.url ~ "^/store/wedding-collapsible-foam-zipper-bottle-cooler.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-zipper-bottle-cooler.html";
                } else if (req.url ~ "^/store/wedding-collapsible-foam-bottle-sleeve.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-foam-bottle-sleeve.html";
                } else if (req.url ~ "^/store/one-color-collapsible-wedding-can-cooler.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-premium-foam-can-cooler.html";
                } else if (req.url ~ "^/store/full-color-collapsible-wedding-can-cooler.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-koozies-can-coolers.html";
                } else if (req.url ~ "^/store/full-color-collapsible-wedding-can-cooler-submit-your-own.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-koozies-can-coolers.html";
                } else if (req.url ~ "^/store/deluxe-collapsible-koozie-can-kooler.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/deluxe-wedding-collapsible-koozie-can-kooler.html";
                } else if (req.url ~ "^/store/choose-product.phtml") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-koozies-can-coolers.html";
                } else if (req.url ~ "^/store/bride-collapsible-can-cooler-full-color.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/store/wedding-party-can-cooler-designs.html";
                } else if (req.url ~ "^/store/black-wedding-party-collapsible-can-cooler.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/store/wedding-party-can-cooler-designs.html";
                } else if (req.url ~ "^/store/home.html/store/funny-contemporary-designs-wedding-koozies.html") {
                        set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/store/design-gallery.html?category=funny";
                } else if (req.url ~ "^/wedding-favors-and-reception.html") {
			set req.http.X-Redirect = "https://www.totallyweddingkoozies.com/wedding-favors.html";
		}
        }

        if (req.http.X-Redirect) {
                return (synth(301, "Moved"));
        }
}
